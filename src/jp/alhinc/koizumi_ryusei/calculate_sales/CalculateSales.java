package jp.alhinc.koizumi_ryusei.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class CalculateSales {;

	public static void main(String[] args)  {

		//エラー処理4-1		コマンドライン引数有無チェック
		if( args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		HashMap<String, String> branchNames = new HashMap<>();
		HashMap<String, Long> branchSales = new HashMap<>();
		HashMap<String, String> commodityNames = new HashMap<>();
		HashMap<String, Long> commoditySales = new HashMap<>();

		//支店定義ファイルの読み込み
		String branchRegex = "^[0-9]+${3}";
		if( !readIn(args[0], "branch.lst", "支店", branchRegex, branchNames, branchSales )) {
			return;
		}
		//商品定義ファイル読み込み
		String commodityRegex = "^[A-Za-z0-9]+${8}";
		if( !readIn(args[0], "commodity.lst", "商品", commodityRegex, commodityNames, commoditySales )) {
			return;
		}

		//売上ファイルの読み込み
		File file = new File(args[0]);
		File[] files = file.listFiles();
		ArrayList<File> rcdFiles = new ArrayList<File>();

		//【条件による選別】8桁の数字.rcdに該当するファイル名をリストに格納
		for (int i = 0; i < files.length; i++) {
			if (files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		//エラー処理3-1		売上ファイル連番チェック
		Collections.sort(rcdFiles);
		for(int j = 0; j < rcdFiles.size() - 1; j++) {

			int former = Integer.parseInt(rcdFiles.get(j).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(j+1).getName().substring(0,8));
			if(latter - former != 1) {
				System.out.println("売上ファイルが連番になっていません");
				return;
			}
		}

		//rcdFilesに格納されているファイルを指定し読み込む
		BufferedReader br = null;
		for (int i = 0; i < rcdFiles.size(); i++) {
			try {
				FileReader fr = new FileReader(files[i]);
				br = new BufferedReader(fr);

				//内容をbranchSalesに格納
				ArrayList<String> items = new ArrayList<String>();
				String line;
				while((line = br.readLine()) != null) {
					items.add(line);
				}

				//エラー処理3-5		売上ファイル行数チェック
				int rawNum = 3;
				if( items.size() != rawNum ) {
					System.out.println(files[i].getName() + "のフォーマットが不正です");
					return;
				}

				//エラー処理3-3		支店定義ファイルと売上ファイルの支店番号チェック
				if( !branchNames.containsKey(items.get(0))) {
					System.out.println(files[i].getName() + "の支店コードが不正です");
					return;
				}

				//エラー処理3-4		商品定義ファイルと売上ファイルの商品番号チェック
				if( !commodityNames.containsKey(items.get(1))) {
					System.out.println(files[i].getName() + "の商品コードが不正です");
					return;
				}

				//エラー処理4-2		売上高の書式チェック
				if( !items.get(2).matches("^[0-9]+$") ) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				//売上を計上する
				Long saleAmount = branchSales.get(items.get(0));
				saleAmount += Long.parseLong(items.get(2));

				//エラー処理3-2		計上売上の上限チェック（10桁以下）
				Long maxSales = 10000000000L;
				if(saleAmount >= maxSales) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				branchSales.replace(items.get(0), saleAmount);
				commoditySales.replace(items.get(1), saleAmount);

			}catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}finally {
				if (br != null) {
					try {
						br.close();
					}catch(IOException e){
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}

		//売上集計ファイルへ出力
		if( !writeOut(args[0], "branch.out", branchNames, branchSales )){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		if( !writeOut(args[0], "commodity.out", commodityNames, commoditySales )){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
	}



	/**
	 * 定義ファイル読み込みメソッド
	 *
	 * @param dirPath	ディレクトリパス
	 * @param lstName	lstファイル名
	 * @param defName	定義ファイル名
	 * @param regex	正規表現
	 * @param namesMap	定義ファイル情報格納Map
	 * @param salesMap	売上ファイル情報格納Map
	 * @return			true:処理続行 false:エラー文出力
	 */
	public static boolean readIn(String dirPath, String lstName, String defName, String regex, HashMap<String, String> namesMap,  HashMap<String, Long> salesMap){
		BufferedReader br = null;
		try {
			File file = new File(dirPath,lstName);
			//エラー処理1-1,2-1		定義ファイルの存在チェック
			if(!file.exists()) {
				System.out.println(defName + "定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			//1行目をkey・2行目をvalueとしてbranchNameへ格納、1行目のみをkeyとしてbranchSaleへ格納
			String line;
			while((line = br.readLine()) != null ) {
				String[] items = line.split(",", -1);

				//エラー処理1-2,2-2		定義ファイルの書式チェック
				if(items.length != 2 || !items[0].matches(regex)) {
					System.out.println(defName + "定義ファイルのフォーマットが不正です");
					return false;
				}

				namesMap.put(items[0], items[1]);
				salesMap.put(items[0], 0L);
			}
		}catch(IOException x) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException x) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 出力メソッド
	 *
	 * @param dirPath		ディレクトリパス
	 * @param outFilename	出力ファイル名
	 * @param namesMap		定義ファイル情報格納Map
	 * @param salesMap		売上ファイル情報格納Map
	 * @return				true:処理続行 false:エラー文出力
	 */
	public static boolean writeOut(String dirPath, String outFilename, HashMap<String, String> namesMap, HashMap<String, Long> salesMap) {
		BufferedWriter bw = null;
		try {
			File outFile = new File(dirPath, outFilename);
			FileWriter fw = new FileWriter(outFile);
			bw = new BufferedWriter(fw);

			for (String key : namesMap.keySet()) {
				bw.write(key + "," + namesMap.get(key) + "," + salesMap.get(key));
				bw.newLine();
			}

		}catch(IOException e){
			 System.out.println("予期せぬエラーが発生しました");
			 return false;
		}finally {
			if (bw != null) {
				try {
					bw.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}
